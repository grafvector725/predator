export class Utils {
  protected constructor() {}

  public static R(min: number, max: number): number {
    const rand = min - 0.5 + Math.random() * (max - min + 1);

    return Math.round(rand);
  }

  public static getIndexByKey(key: string): { i: number; j: number } {
    const coordsList = key.split("_").map(Number);

    return { i: coordsList[0], j: coordsList[1] };
  }

  public static getKeyByIndex(i: number, j: number): string {
    return `${i}_${j}`;
  }
}

import * as React from "react";
import { squareStyle } from "./Square.styles";
import { boundMethod } from "autobind-decorator";

interface ISquareProps {
  i: number;
  j: number;
  regSquare(i: number, j: number, value: number[], lastElement: boolean): void;
  limitCount?: number;
}

interface ISquareState {
  x: number;
  y: number;
}

class Square extends React.PureComponent<ISquareProps, ISquareState> {
  private container: React.RefObject<HTMLDivElement> | null = null;
  public readonly state: ISquareState = {
    x: -1,
    y: -1,
  };

  constructor(props: ISquareProps) {
    super(props);

    this.container = React.createRef();
  }

  public componentDidMount() {
    const { i, j, regSquare, limitCount } = this.props;

    const isLastElement = i === limitCount && j === limitCount;

    regSquare(i, j, [this.getTop(), this.getLeft()] as number[], isLastElement);
  }

  public componentDidUpdate() {}

  @boundMethod
  private handleClick(): void {
    const { i, j } = this.props;

    console.warn(`${i}_${j}`);
  }

  private getTop(): number {
    const { i } = this.props;

    return i !== 0 ? i * 25 + 32 + i * 6 : i * 25 + 32;
  }

  private getLeft(): number {
    const { j } = this.props;

    return j !== 0 ? j * 25 + 32 + j * 6 : j * 25 + 32;
  }

  public render() {
    return (
      <div
        ref={this.container}
        className={squareStyle}
        onClick={this.handleClick}
        style={{
          top: `${this.getTop()}px`,
          left: `${this.getLeft()}px`,
        }}
      />
    );
  }
}

export default Square;

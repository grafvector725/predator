import * as React from "react";
// import { TCoords } from "../../models/models.types";
import Woolf from "../../models/Woolf/Woolf";
import { default as AnimalModel } from "../../models/Animal/Animal";
import Rabbit from "../../models/Rabbit/Rabbit";
import { rabbitStyle, woolfManStyle, woolfWomanStyle } from "./Animal.styles";
import { Utils } from "../../utils/Utils";
import { boundMethod } from "autobind-decorator";
import RabbitSVG from "../../rabbit.svg";
import WoolfSVG from "../../woolf.svg";

interface IAnimalProps {
  model: AnimalModel;
  iIndex: number;
  jIndex: number;
  coordsList: Map<string, number[]>;
  otherAnimals: Map<string, AnimalModel>;
  createEntity(Model: typeof Woolf | typeof Rabbit): void;
  destroyEntity(key: string): void;
}
interface IAnimalState {
  currentCoordsIndexKey: string;
}

type TCoordsOnPolygon = {
  key: string;
  coords: number[];
};

class Animal extends React.PureComponent<IAnimalProps, IAnimalState> {
  constructor(props: IAnimalProps) {
    super(props);

    this.state = {
      currentCoordsIndexKey: Utils.getKeyByIndex(
        this.props.iIndex,
        this.props.jIndex
      ),
    };
  }

  public componentDidMount() {
    setInterval(this.updatePositions, 1000);
  }

  public componentWillUnmount() {
    clearInterval();
  }

  private setAvailablePath(
    i: number,
    j: number,
    pathList: Map<number, TCoordsOnPolygon>
  ): Map<number, TCoordsOnPolygon> {
    const key = Utils.getKeyByIndex(i, j);
    const item = this.props.coordsList.get(key);

    return !!item
      ? pathList.set(pathList.size, { key, coords: item })
      : pathList;
  }

  private getAvailableWays(
    key: string,
    offset: number = 8
  ): Map<number, TCoordsOnPolygon> {
    const availableList: Map<number, TCoordsOnPolygon> = new Map();
    const { i, j } = Utils.getIndexByKey(key);

    for (let k = 0; k < offset; k++) {
      for (let n = 0; n < offset; n++) {
        this.setAvailablePath(i, n, availableList);
        this.setAvailablePath(k, j, availableList);

        this.setAvailablePath(i - k, j - n, availableList);
        this.setAvailablePath(i - k, j + n, availableList);

        this.setAvailablePath(i + k, j + n, availableList);
        this.setAvailablePath(i - k, n, availableList);

        this.setAvailablePath(i + k, j - n, availableList);
        this.setAvailablePath(i, j - n, availableList);
      }
    }

    return availableList;
  }

  private checkExistItemInCoordsAndGet(
    i: number,
    j: number,
    pathList: Map<string, number[]>
  ): Map<string, number[]> {
    const key = Utils.getKeyByIndex(i, j);
    const item = this.props.coordsList.get(key);

    return !!item ? pathList.set(key, item) : pathList;
  }

  private getAvailableWaysAsCoordsList(
    key: string,
    offset: number = 8
  ): Map<string, number[]> {
    const availableList: Map<string, number[]> = new Map();
    const { i, j } = Utils.getIndexByKey(key);

    for (let k = 0; k < offset; k++) {
      for (let n = 0; n < offset; n++) {
        this.checkExistItemInCoordsAndGet(i, n, availableList);
        this.checkExistItemInCoordsAndGet(k, j, availableList);

        this.checkExistItemInCoordsAndGet(i - k, j - n, availableList);
        this.checkExistItemInCoordsAndGet(i - k, j + n, availableList);

        this.checkExistItemInCoordsAndGet(i + k, j + n, availableList);
        this.checkExistItemInCoordsAndGet(i - k, n, availableList);

        this.checkExistItemInCoordsAndGet(i + k, j - n, availableList);
        this.checkExistItemInCoordsAndGet(i, j - n, availableList);
      }
    }

    return availableList;
  }

  @boundMethod
  private updatePositions() {
    const { model } = this.props;

    const availablePaths = this.getAvailableWays(
      this.state.currentCoordsIndexKey
    );
    const randKey = Utils.R(0, availablePaths.size - 1);
    const randPath = availablePaths.get(randKey);
    const newPosition = randPath?.coords;

    model.setCoords({ x: newPosition?.[0], y: newPosition?.[1] });
    this.setState(
      {
        currentCoordsIndexKey: randPath?.key!!,
      },
      this.checkAround
    );
  }

  @boundMethod
  private checkAround(): void {
    const { model, otherAnimals } = this.props;
    const { currentCoordsIndexKey } = this.state;

    otherAnimals.forEach((value, key) => {
      const availablePaths = this.getAvailableWaysAsCoordsList(key);
      const existItem = availablePaths.get(currentCoordsIndexKey);

      if (
        existItem &&
        value instanceof Rabbit &&
        model instanceof Rabbit &&
        key === currentCoordsIndexKey
      ) {
        this.props.createEntity(Rabbit as any);
      }

      if (existItem && model instanceof Woolf && value instanceof Rabbit) {
        this.props.destroyEntity(currentCoordsIndexKey);
      }

      if (
        existItem &&
        value instanceof Woolf &&
        model instanceof Woolf &&
        key === currentCoordsIndexKey
      ) {
      }
    });
  }

  private getClassNameByModel(): string {
    const { model } = this.props;

    if (model instanceof Rabbit) {
      return rabbitStyle;
    }

    if (model instanceof Woolf) {
      if (model.getGender() === "MAN") {
        return woolfManStyle;
      }

      if (model.getGender() === "WOMAN") {
        return woolfWomanStyle;
      }
    }

    return "";
  }

  private getIcon(): React.ReactNode {
    const { model } = this.props;

    if (model instanceof Rabbit) {
      return <img src={RabbitSVG} alt="rabbit" />;
    }

    if (model instanceof Woolf) {
      return <img src={WoolfSVG} alt="woolf" />;
    }

    return null;
  }

  public render() {
    const { model } = this.props;
    return (
      <span
        className={this.getClassNameByModel()}
        style={{ top: model.getOX(), left: model.getOY() }}
      >
        {this.getIcon()}
      </span>
    );
  }
}

export default Animal;

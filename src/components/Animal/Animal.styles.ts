import { style } from "typestyle";

export const baseStyle = {
  width: 32,
  height: 32,
  position: "absolute",
  background: "red",
} as const;

export const woolfManStyle = style({ ...baseStyle, background: "red" });
export const woolfWomanStyle = style({ ...baseStyle, background: "pink" });
export const rabbitStyle = style({ ...baseStyle, background: "green" });

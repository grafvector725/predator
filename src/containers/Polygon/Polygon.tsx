import * as React from "react";
import Square from "../../components/Square/Square";
import { polygonStyle } from "./Polygon.styles";
import { boundMethod } from "autobind-decorator";
// import Woolf from "../../models/Woolf/Woolf";
import Rabbit from "../../models/Rabbit/Rabbit";
import { default as AnimalModel } from "../../models/Animal/Animal";
import Animal from "../../components/Animal/Animal";

import { Utils } from "../../utils/Utils";
import Woolf from "../../models/Woolf/Woolf";

interface IPolygonProps {
  size: number;
  countRabbit: number;
  countWoolfWoman: number;
  countWoolf: number;
  start: boolean;
}
interface IPolygonState {
  loadingComplete: boolean;
}

class Polygon extends React.PureComponent<IPolygonProps, IPolygonState> {
  private coordsList: Map<string, number[]> = new Map();
  private faunaList: Map<string, AnimalModel> = new Map();

  public readonly state: IPolygonState = {
    loadingComplete: false,
  };

  public componentDidUpdate(
    prevProps: IPolygonProps,
    prevState: IPolygonState
  ): void {
    if (!prevProps.start && this.props.start) {
      this.setFauna();
    }

    if (prevProps.start && !this.props.start) {
      this.faunaList.clear();
    }
  }

  @boundMethod
  private updateMap(
    i: number,
    j: number,
    value: number[],
    lastElement = false
  ): void {
    const key = Utils.getKeyByIndex(i, j);

    this.setState({ loadingComplete: lastElement });
    this.coordsList.set(key, value);
  }

  @boundMethod
  private setFauna(): void {
    const { countRabbit, countWoolf, countWoolfWoman } = this.props;

    for (let i = 0; i < countRabbit; i++) {
      const randI = Utils.R(i, 19);
      const randJ = Utils.R(i, 19);
      const key: string = Utils.getKeyByIndex(randI, randJ);

      const randCoords: number[] | undefined = this.coordsList.get(key);

      const coords = {
        x: randCoords?.[0] || -1,
        y: randCoords?.[1] || -1,
      };

      const rabbit: AnimalModel = new Rabbit(coords);
      this.faunaList.set(key, rabbit);
    }

    for (let i = 0; i < countWoolf; i++) {
      const randI = Utils.R(i, 19);
      const randJ = Utils.R(i, 19);
      const key: string = Utils.getKeyByIndex(randI, randJ);

      const randCoords: number[] | undefined = this.coordsList.get(key);

      const coords = {
        x: randCoords?.[0] || -1,
        y: randCoords?.[1] || -1,
      };

      const woolf: AnimalModel = new Woolf(coords, "MAN");
      this.faunaList.set(key, woolf);
    }

    for (let i = 0; i < countWoolfWoman; i++) {
      const randI = Utils.R(i, 19);
      const randJ = Utils.R(i, 19);
      const key: string = Utils.getKeyByIndex(randI, randJ);

      const randCoords: number[] | undefined = this.coordsList.get(key);

      const coords = {
        x: randCoords?.[0] || -1,
        y: randCoords?.[1] || -1,
      };

      const woolf: AnimalModel = new Woolf(coords, "WOMAN");
      this.faunaList.set(key, woolf);
    }
    this.forceUpdate();
  }

  @boundMethod
  private createEntity(Clazz: typeof Woolf | typeof Rabbit): void {
    const randI = Utils.R(0, 19);
    const randJ = Utils.R(0, 19);
    const key: string = Utils.getKeyByIndex(randI, randJ);
    if (!this.faunaList.has(key)) {
      const randCoords: number[] | undefined = this.coordsList.get(key);

      const coords = {
        x: randCoords?.[0] || -1,
        y: randCoords?.[1] || -1,
      };
      const rabbit: AnimalModel = new Clazz(coords);
      this.faunaList.set(key, rabbit);

      this.forceUpdate();
    }
  }

  @boundMethod
  private destroyEntity(key: string): void {
    this.faunaList.delete(key);

    this.forceUpdate();
  }

  private getPolygon(): React.ReactNode[] {
    const polygon: React.ReactNode[] = [];
    const { size } = this.props;

    for (let i = 0; i < size; i++) {
      const line: React.ReactNode[] = [];

      for (let j = 0; j < size; j++) {
        line.push(
          <Square
            key={`${i}_${j}`}
            i={i}
            j={j}
            regSquare={this.updateMap}
            limitCount={size - 1}
          />
        );
      }

      polygon.push(line);
      polygon.push(<br key={i} />);
    }

    return polygon;
  }

  private getFauna(): React.ReactNode[] {
    const fauna: React.ReactNode[] = [];
    this.faunaList.forEach((animal, key) => {
      const { i, j } = Utils.getIndexByKey(key);

      fauna.push(
        <Animal
          key={animal.toString()}
          model={animal}
          iIndex={i}
          jIndex={j}
          coordsList={this.coordsList}
          otherAnimals={this.faunaList}
          createEntity={this.createEntity}
          destroyEntity={this.destroyEntity}
        />
      );
    });

    return fauna;
  }

  public render() {
    const { start } = this.props;
    return (
      <>
        <div className={polygonStyle}>
          {this.getPolygon()}
          {start ? this.getFauna() : null}
        </div>
      </>
    );
  }
}

export default Polygon;

import React from "react";
import Polygon from "./containers/Polygon/Polygon";
import { style } from "typestyle";
import { boundMethod } from "autobind-decorator";

const labelStyle = style({
  display: "flex",
  flex: "1 0 220px",
});

const rowStyle = style({
  display: "flex",
  height: "36px",
  flexWrap: "wrap",
  alignItems: "center",
  flexGrow: 1,
});

const inputStyle = style({
  display: "flex",
  flex: "1 0 220px",
  border: "1px solid black",
  height: "24px",
  borderRadius: "4px",
  padding: "2 4 2 4",
});

const submitStyle = style({
  width: "164px",
  float: "right",
  textAlign: "center",
  border: "1px solid black",
  borderRadius: "4px",
  padding: "8px",
  lineHeight: "12px",
  cursor: "pointer",
  marginTop: "8px",
});

class App extends React.PureComponent<
  any,
  {
    countRabbit: number;
    countWoolfWoman: number;
    countWoolf: number;
    start: boolean;
  }
> {
  public readonly state = {
    countRabbit: 0,
    countWoolfWoman: 0,
    countWoolf: 0,
    start: false,
  };

  @boundMethod
  private handleChangeRabbit(e: any): void {
    this.setState({ countRabbit: e.target?.value, start: false });

    e.preventDefault();
  }

  @boundMethod
  private handleChangeWoolf(e: any): void {
    this.setState({ countWoolf: e.target?.value, start: false });

    e.preventDefault();
  }

  @boundMethod
  private handleChangeWoolfWoman(e: any): void {
    this.setState({ countWoolfWoman: e.target?.value, start: false });

    e.preventDefault();
  }

  @boundMethod
  private handleStart(e: any): void {
    this.setState({ start: true });

    e.preventDefault();
  }

  public render() {
    return (
      <>
        <div key="form" style={{ width: "50%", padding: "16px" }}>
          <div className={rowStyle}>
            <label htmlFor="rabbits" className={labelStyle}>
              Кол-во кроликов
            </label>
            <input
              name="rabbits"
              className={inputStyle}
              type="number"
              onChange={this.handleChangeRabbit}
            />
          </div>

          <div className={rowStyle}>
            <label htmlFor="woolf_man" className={labelStyle}>
              Кол-во волков
            </label>
            <input
              name="woolf_man"
              className={inputStyle}
              type="number"
              onChange={this.handleChangeWoolf}
            />
          </div>

          <div className={rowStyle}>
            <label htmlFor="woolf_woman" className={labelStyle}>
              Кол-во волчиц
            </label>
            <input
              name="woolf_woman"
              className={inputStyle}
              type="number"
              onChange={this.handleChangeWoolfWoman}
            />
          </div>

          <div className={submitStyle} onClick={this.handleStart}>
            Start
          </div>
        </div>
        <Polygon
          key="polygon"
          size={20}
          countRabbit={this.state.countRabbit}
          countWoolf={this.state.countWoolf}
          countWoolfWoman={this.state.countWoolfWoman}
          start={this.state.start}
        />
        ,
      </>
    );
  }
}

export default App;

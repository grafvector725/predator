import { TCoords } from "../models.types";
import { isUndefined } from "util";

class Animal {
  protected coords: TCoords;

  protected constructor(coords: TCoords) {
    this.coords = coords;
  }

  public setCoords(coords: TCoords): void {
    this.coords = { ...coords };

    if (isUndefined(coords.x) || isUndefined(coords.y)) {
      throw new Error("Check your coords");
    }
  }

  public getCords(): TCoords {
    return this.coords;
  }

  public getOX(): number | undefined {
    return this.coords.x;
  }

  public getOY(): number | undefined {
    return this.coords.y;
  }

  public toString(): string {
    return `${this.coords.x}-${this.coords.y}`;
  }
}

export default Animal;

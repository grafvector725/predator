import { TCoords } from "../models.types";
import Animal from "../Animal/Animal";

export type TGender = "MAN" | "WOMAN";

class Woolf extends Animal {
  private gender: TGender;
  private rating: number;

  public constructor(coords: TCoords, gender: TGender = "MAN") {
    super(coords);
    this.gender = gender;
    this.rating = 2;
  }

  public setRating(rating: number): void {
    this.rating = rating;
  }

  public getGender(): TGender {
    return this.gender;
  }

  public getRating(): number {
    return this.rating;
  }

  public toString(): string {
    return super.toString() + this.rating + this.gender + "Woolf";
  }
}

export default Woolf;

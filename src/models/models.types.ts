export type TCoords = {
  x: number | undefined;
  y: number | undefined;
};

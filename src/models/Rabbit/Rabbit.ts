import { TCoords } from "../models.types";
import Animal from "../Animal/Animal";

class Rabbit extends Animal {
  public constructor(coords: TCoords) {
    super(coords);
  }

  public getCoords(): TCoords {
    return this.coords;
  }

  public toString(): string {
    return super.toString() + "- Rabbit";
  }
}

export default Rabbit;
